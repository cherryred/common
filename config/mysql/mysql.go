package mysql

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Config struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	DBName   string `json:"dbname"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (m *Config) NewMysqlDb() *gorm.DB {
	db, err := gorm.Open(mysql.Open(m.ToDsn()), &gorm.Config{})
	if err != nil {
		fmt.Printf("init service context failed %s", err)
		return nil
	}
	return db
}

func (m *Config) ToDsn() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username, m.Password, m.Host, m.Port, m.DBName)
}
