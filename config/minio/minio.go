package minio

import (
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

type Config struct {
	Endpoint        string `json:"endpoint"`
	AccessKey       string `json:"accessKey"`
	SecretAccessKey string `json:"secretAccessKey"`
	Location        string `json:"location"`
}

func (m *Config) NewClient() *minio.Client {
	minioClient, err := minio.New(m.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(m.AccessKey, m.SecretAccessKey, ""),
		Secure: false,
	})
	if err != nil {
		fmt.Printf("new minio client failed. err %s\n", err.Error())
		return minioClient
	}
	fmt.Println("new minio client success.")
	return minioClient
}
