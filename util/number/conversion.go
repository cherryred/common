package number

import (
	"fmt"
	"strconv"
)

func exampleDecimalToBase() {
	decimalNumber := 123456789 // 10进制数
	base64Number := DecimalToBase64(decimalNumber)
	fmt.Println(base64Number)
}

func decimalToBaseN(decimal int, baseN int, baseChars string) string {
	result := ""
	for decimal > 0 {
		remainder := decimal % baseN
		result = string(baseChars[remainder]) + result
		decimal = decimal / baseN
	}
	return result
}

func DecimalToBase2(decimal int) string {
	const base2Chars = "01"
	return decimalToBaseN(decimal, 2, base2Chars)
}

func DecimalToBase8(decimal int) string {
	const base8Chars = "01234567"
	return decimalToBaseN(decimal, 8, base8Chars)
}

func DecimalToBaseId(decimal int) string {
	const base62Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	return decimalToBaseN(decimal, 62, base62Chars)
}

func DecimalToBase64(decimal int) string {
	const base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	return decimalToBaseN(decimal, 64, base64Chars)
}

func IntToString(v int) string {
	return strconv.Itoa(v)
}

func Int64ToString(v int64) string {
	return strconv.FormatInt(v, 10)
}

func StringToInt(v string) int {
	val, err := strconv.Atoi(v)
	if err != nil {
		panic("StringToInt invalid")
	}
	return val
}

func StringToInt64(v string) int64 {
	val, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		panic("StringToInt invalid")
	}
	return val
}
