package user

import (
	"context"
	"fmt"

	"gitee.com/cherryred/common/constant"
)

func GetUserId(ctx context.Context) constant.UserIdType {
	return 0
}

func GetStringUserId(ctx context.Context) string {
	return ctx.Value("userId").(string)
}

func ToString(value interface{}) string {
	if v, ok := value.(string); ok {
		return v
	}
	fmt.Printf("to string failed, value %v\n", value)
	return ""
}
