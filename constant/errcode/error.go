package errcode

type BaseError struct {
	code  int64
	value string
}

func NewError(code int64, value string) error {
	return &BaseError{code: code, value: value}
}

func (b *BaseError) Error() string {
	return b.value
}

func (b *BaseError) Code() int64 {
	return b.code
}
